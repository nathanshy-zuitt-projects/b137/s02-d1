package b137.shy.s02d1;

public class ArraysAndOperators {

    public static void main(String[] args) {
        System.out.print("Arrays and Operators\n");

        // Declare an array of integers

        int[] arrayOfNumbers = new int[5];

        System.out.println("The element at index 0 is: " + arrayOfNumbers[0]);
        System.out.println("The element at index 1 is: " + arrayOfNumbers[1]);
        System.out.println("The element at index 2 is: " + arrayOfNumbers[2]);
        System.out.println("The element at index 3 is: " + arrayOfNumbers[3]);
        System.out.println("The element at index 4 is: " + arrayOfNumbers[4]);

        // Mini-activity: assign the ff values to the elements of arrayOfNumbers
        // 30, 11, 23, 35, 0
        // Start assigning wit the element at index 0 and move ff respectively

        int[] assignedArrayOfNumbers = new int[]{30, 11, 23, 55, 0};

        for (int i = 0; i < arrayOfNumbers.length; i++) {
            System.out.println("Element at index " + i + " is " + assignedArrayOfNumbers[i]);
        }

        // Declare an array of an arbitrary size
        int[] arrayOfArbitraryNumbers = {};

    }
}
